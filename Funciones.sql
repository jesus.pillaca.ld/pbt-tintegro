﻿
-- Listar Departamentos
CREATE OR REPLACE FUNCTION "Admision".listar_departamentos()
RETURNS TABLE (descripcion_departamento VARCHAR)
LANGUAGE plpgsql
AS $$
BEGIN
    RETURN QUERY SELECT DISTINCT tc_ubigeo.descripcion_departamento
                 FROM "Admision".tc_ubigeo
                 ORDER BY tc_ubigeo.descripcion_departamento ASC;
END;
$$;

SELECT * FROM  "Admision".listar_departamentos();

-- Listar Provincia Segun Departamento
CREATE OR REPLACE FUNCTION "Admision".listar_provincias_por_departamento(departamento VARCHAR)
RETURNS TABLE (descripcion_provincia VARCHAR)
LANGUAGE plpgsql
AS $$
BEGIN
    RETURN QUERY 
    SELECT DISTINCT tc_ubigeo.descripcion_provincia
    FROM "Admision".tc_ubigeo
    WHERE tc_ubigeo.descripcion_departamento LIKE '%' || departamento || '%'
    AND tc_ubigeo.descripcion_provincia IS NOT NULL
    AND tc_ubigeo.descripcion_provincia != ''
    ORDER BY tc_ubigeo.descripcion_provincia ASC;
END;
$$;

SELECT "Admision".listar_provincias_por_departamento('Lima');

DROP FUNCTION "Admision".listar_ubigeo_por_provincia_distrito;
-- Litar Ubigeo Distrito
CREATE OR REPLACE FUNCTION "Admision".listar_ubigeo_por_provincia_distrito(provincia VARCHAR)
RETURNS TABLE (
    descripcion_departamento VARCHAR(200),
    descripcion_provincia VARCHAR(200),
    descripcion_distrito VARCHAR(200),
    fl_estado BIT,
    codigo_ubigeo CHAR(6),
    codigo_departamento VARCHAR(2),
    codigo_provincia VARCHAR(2),
    codigo_distrito VARCHAR(2)
)
LANGUAGE plpgsql
AS $$
BEGIN
    RETURN QUERY 
    SELECT DISTINCT
        tc_ubigeo.descripcion_departamento,
        tc_ubigeo.descripcion_provincia,
        tc_ubigeo.descripcion_distrito,
        tc_ubigeo.fl_estado,
        tc_ubigeo.codigo_ubigeo,
        tc_ubigeo.codigo_departamento,
        tc_ubigeo.codigo_provincia,
        tc_ubigeo.codigo_distrito
    FROM "Admision".tc_ubigeo
    WHERE tc_ubigeo.descripcion_provincia LIKE '%' || provincia || '%'
    AND tc_ubigeo.descripcion_distrito IS NOT NULL
    AND tc_ubigeo.descripcion_distrito != ''
    ORDER BY tc_ubigeo.descripcion_distrito ASC;
END;
$$;



SELECT * FROM "Admision".listar_ubigeo_por_provincia_distrito('Lima ');

-- TIpo Documento

CREATE OR REPLACE FUNCTION "Admision".listar_tipos_documento()
RETURNS TABLE (id_tipo_documento_identidad integer, descripcion_tipo_documento character varying, codigo_ieds character(2))
LANGUAGE plpgsql
AS $$
BEGIN
    RETURN QUERY SELECT t.id_tipo_documento_identidad, t.descripcion_tipo_documento_identidad, t.codigo_ieds
                 FROM "Admision".tc_tipo_documento_identidad t
                 WHERE t.fl_estado = '1';
END;
$$;


SELECT * FROM "Admision".listar_tipos_documento();

SELECT *  FROM "Admision".buscar_pacientes(1,'70414141','car','cace','car');
DROP FUNCTION "Admision".buscar_pacientes;
CREATE OR REPLACE FUNCTION "Admision".buscar_pacientes(
    idTipoDoc integer DEFAULT 0,
    noDoc character varying DEFAULT '0',
    apePat character varying DEFAULT '0',
    apeMat character varying DEFAULT '0',
    nombres character varying DEFAULT '0'
)
RETURNS TABLE (
    id_paciente integer,
    id_tipo_docide integer,
    no_docide character varying,
    no_apepat character varying,
    no_apemat character varying,
    no_nombres character varying,
    id_sexo integer,
    fe_nacimiento date,
    no_lugar_nacimiento character varying,
    no_direccion character varying,
    co_ubigeo character varying(6)
) AS $$
BEGIN
    RETURN QUERY
    SELECT *
    FROM "Admision".tb_paciente t
    WHERE
        (idTipoDoc = 0 OR t.id_tipo_docide = idTipoDoc)
        AND (noDoc = '0' OR t.no_docide = noDoc)
        AND (apePat = '0' OR t.no_apepat ILIKE '%' || apePat || '%')
        AND (apeMat = '0' OR t.no_apemat ILIKE '%' || apeMat || '%')
        AND (nombres = '0' OR t.no_nombres ILIKE '%' || nombres || '%');
END;
$$ LANGUAGE plpgsql;

SELECT *  FROM "Admision".tc_sexo ts;


CREATE OR REPLACE FUNCTION "Admision".insertar_paciente(
    idTipoDocumento integer,
    numeroDocumento character varying,
    apellidoPaterno character varying,
    apellidoMaterno character varying,
    nombres character varying,
    idSexo integer,
    fechaNacimiento date,
    lugarNacimiento character varying,
    direccion character varying,
    ubigeo character varying(6)
)
RETURNS INTEGER AS
$$
DECLARE
    paciente_id INTEGER;
BEGIN
    INSERT INTO "Admision".tb_paciente (
        id_tipo_docide,
        no_docide,
        no_apepat,
        no_apemat,
        no_nombres,
        id_sexo,
        fe_nacimiento,
        no_lugar_nacimiento,
        no_direccion,
        co_ubigeo
    ) VALUES (
        idTipoDocumento,
        numeroDocumento,
        apellidoPaterno,
        apellidoMaterno,
        nombres,
        idSexo,
        fechaNacimiento,
        lugarNacimiento,
        direccion,
        ubigeo
    ) RETURNING id_paciente INTO paciente_id;

    RETURN paciente_id;
END;
$$
LANGUAGE plpgsql;

DROP FUNCTION "Admision".insertar_acompanante;

--insertar acompañante:
CREATE OR REPLACE FUNCTION "Admision".insertar_acompanante(
    idPacienteA integer,
    idTipoDocumentoA integer,
    numeroDocumentoA character varying,
    apellidoPaternoA character varying,
    apellidoMaternoA character varying,
    nombresA character varying,
    fechaNacimientoA date,
    idParentescoA integer,
    telefonoContactoA character varying,
    direccionA character varying,
    ubigeoA character varying(6)
)
RETURNS INTEGER AS
$$
DECLARE
    acompanante_id INTEGER;
BEGIN
    INSERT INTO "Admision".tb_paciente_acompanante (
        id_paciente,
        id_tipo_docide,
        no_docide,
        no_apepat,
        no_apemat,
        no_nombres,
        fe_nacimiento,
        id_parentesco,
        nu_telefo_contacto,
        no_direccion,
        co_ubigeo
    ) VALUES (
        idPacienteA,
        idTipoDocumentoA,
        numeroDocumentoA,
        apellidoPaternoA,
        apellidoMaternoA,
        nombresA,
        fechaNacimientoA,
        idParentescoA,
        telefonoContactoA,
        direccionA,
        ubigeoA
    ) RETURNING id_paciente_acompanante INTO acompanante_id;

    RETURN acompanante_id;
END;
$$
LANGUAGE plpgsql;
