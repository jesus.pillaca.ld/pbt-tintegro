package com.jpillaca.apipacientesti.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SexoDTO {
    private Integer idSexo;
    private String descripcionSexo;
    private Boolean flEstado;
}