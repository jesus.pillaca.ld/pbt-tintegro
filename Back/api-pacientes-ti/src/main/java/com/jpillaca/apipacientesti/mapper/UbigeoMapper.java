package com.jpillaca.apipacientesti.mapper;

import com.jpillaca.apipacientesti.model.Ubigeo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UbigeoMapper {

    @Select("SELECT * FROM \"Admision\".listar_departamentos()")
    List<String> listarDepartamentos();

    @Select("SELECT * FROM \"Admision\".listar_provincias_por_departamento(#{departamento})")
    List<String> listarProvinciasPorDepartamento(@Param("departamento") String departamento);

    @Select("SELECT * FROM \"Admision\".listar_ubigeo_por_provincia_distrito(#{provincia})")
    @Results({
            @Result(property = "descripcionDepartamento", column = "descripcion_departamento"),
            @Result(property = "descripcionProvincia", column = "descripcion_provincia"),
            @Result(property = "descripcionDistrito", column = "descripcion_distrito"),
            @Result(property = "flEstado", column = "fl_estado"),
            @Result(property = "codigoUbigeo", column = "codigo_ubigeo"),
            @Result(property = "codigoDepartamento", column = "codigo_departamento"),
            @Result(property = "codigoProvincia", column = "codigo_provincia"),
            @Result(property = "codigoDistrito", column = "codigo_distrito")
    })
    List<Ubigeo> listarUbigeoPorProvinciaDistrito(@Param("provincia") String provincia);
}
