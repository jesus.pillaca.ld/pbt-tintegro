package com.jpillaca.apipacientesti.mapper;

import com.jpillaca.apipacientesti.model.Sexo;
import com.jpillaca.apipacientesti.model.TipoDocumento;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SexoMapper {
    @Select("SELECT * FROM \"Admision\".tc_sexo")
    @Results({
            @Result(property = "idSexo", column = "id_sexo"),
            @Result(property = "descripcionSexo", column = "descripcion_sexo"),
            @Result(property = "flEstado", column = "fl_estado")
    })
    List<Sexo> getAllSexo();
}
