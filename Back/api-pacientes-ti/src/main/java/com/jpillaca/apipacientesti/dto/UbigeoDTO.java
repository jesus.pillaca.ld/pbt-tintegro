package com.jpillaca.apipacientesti.dto;

import lombok.Data;

@Data
public class UbigeoDTO {
    private String descripcionDistrito;
    private String codigoUbigeo;
}
