package com.jpillaca.apipacientesti.controller;

import com.jpillaca.apipacientesti.dto.TipoDocumentoDTO;
import com.jpillaca.apipacientesti.service.TipoDocumentoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/tipo-documento")
public class TipoDocumentoController {
    private final TipoDocumentoService tipoDocumentoService;
    public TipoDocumentoController(TipoDocumentoService tipoDocumentoService) {
        this.tipoDocumentoService = tipoDocumentoService;
    }
    @GetMapping("/all")
    public List<TipoDocumentoDTO> getAllTiposDocumento() {
        return tipoDocumentoService.getAllTiposDocumento();
    }
}
