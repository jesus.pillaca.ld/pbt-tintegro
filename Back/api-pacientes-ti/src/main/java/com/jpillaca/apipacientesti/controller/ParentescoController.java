package com.jpillaca.apipacientesti.controller;

import com.jpillaca.apipacientesti.dto.ParentescoDTO;
import com.jpillaca.apipacientesti.service.ParentescoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/parentesco")
public class ParentescoController {
    private final ParentescoService parentescoService;
    public ParentescoController(ParentescoService parentescoServices) {
        this.parentescoService = parentescoServices;
    }
    @GetMapping("/all")
    public List<ParentescoDTO> getAllTiposDocumento() {
        return parentescoService.getAllParentesco();
    }
}
