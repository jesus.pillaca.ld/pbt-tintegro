package com.jpillaca.apipacientesti.controller;

import com.jpillaca.apipacientesti.dto.UbigeoDTO;
import com.jpillaca.apipacientesti.service.UbigeoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/ubigeo")
public class UbigeoController {

    private final UbigeoService ubigeoService;

    public UbigeoController(UbigeoService ubigeoService) {
        this.ubigeoService = ubigeoService;
    }

    @GetMapping("/departamentos")
    public List<String> listarDepartamentos() {
        return ubigeoService.listarDepartamentos();
    }

    @GetMapping("/provincias/{departamento}")
    public List<String> listarProvinciasPorDepartamento(@PathVariable String departamento) {
        return ubigeoService.listarProvinciasPorDepartamento(departamento);
    }

    @GetMapping("/ubigeo/{provincia}")
    public List<UbigeoDTO> listarUbigeoPorProvinciaDistrito(@PathVariable String provincia) {
        return ubigeoService.listarUbigeoPorProvinciaDistrito(provincia);
    }
}