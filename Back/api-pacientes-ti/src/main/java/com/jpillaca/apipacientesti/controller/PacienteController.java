package com.jpillaca.apipacientesti.controller;

import com.jpillaca.apipacientesti.model.Paciente;
import com.jpillaca.apipacientesti.service.PacienteService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/paciente")
public class PacienteController {

    private final PacienteService pacienteService;


    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    @GetMapping("/find")
    public List<Paciente> buscarPacientes(@RequestParam(required = false) Integer idTipoDoc,
                                          @RequestParam(required = false) String noDoc,
                                          @RequestParam(required = false) String apePat,
                                          @RequestParam(required = false) String apeMat,
                                          @RequestParam(required = false) String nombres) {
        return pacienteService.buscarPacientes(idTipoDoc, noDoc, apePat, apeMat, nombres);
    }
}