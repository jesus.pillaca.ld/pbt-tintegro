package com.jpillaca.apipacientesti.service.impl;

import com.jpillaca.apipacientesti.dto.SexoDTO;
import com.jpillaca.apipacientesti.dto.SexoDTO;
import com.jpillaca.apipacientesti.mapper.SexoMapper;
import com.jpillaca.apipacientesti.model.Sexo;
import com.jpillaca.apipacientesti.model.Sexo;
import com.jpillaca.apipacientesti.model.TipoDocumento;
import com.jpillaca.apipacientesti.service.SexoService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SexoServiceImp implements SexoService {
    private final SexoMapper sexoMapper;

    public SexoServiceImp(SexoMapper sexoMapper) {
        this.sexoMapper = sexoMapper;
    }

    @Override
    public List<SexoDTO> getAllSexo() {
        List<Sexo> sexo = sexoMapper.getAllSexo();
        return sexoModelToDTO(sexo);
    }

    private List<SexoDTO> sexoModelToDTO(List<Sexo> sexoin) {
        List<SexoDTO> dtos = new ArrayList<>();
        for (Sexo sexo : sexoin) {
            SexoDTO dto = new SexoDTO();
            dto.setIdSexo(sexo.getIdSexo());
            dto.setDescripcionSexo(sexo.getDescripcionSexo());
            dto.setFlEstado(sexo.getFlEstado());
            dtos.add(dto);
        }
        return dtos;
    }
}
