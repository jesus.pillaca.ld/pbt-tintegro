package com.jpillaca.apipacientesti.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParentescoDTO {
    private Integer idParentesco;
    private String noParentesco;
    private Boolean ilActivo;
}
