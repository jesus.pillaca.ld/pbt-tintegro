package com.jpillaca.apipacientesti.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Parentesco {
    private Integer idParentesco;
    private String noParentesco;
    private Boolean ilActivo;
}
