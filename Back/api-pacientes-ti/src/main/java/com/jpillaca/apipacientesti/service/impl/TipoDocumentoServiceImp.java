package com.jpillaca.apipacientesti.service.impl;

import com.jpillaca.apipacientesti.dto.TipoDocumentoDTO;
import com.jpillaca.apipacientesti.mapper.TipoDocumentoMapper;
import com.jpillaca.apipacientesti.model.TipoDocumento;
import com.jpillaca.apipacientesti.service.TipoDocumentoService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TipoDocumentoServiceImp implements TipoDocumentoService {

    private final TipoDocumentoMapper tipoDocumentoMapper;

    public TipoDocumentoServiceImp(TipoDocumentoMapper tipoDocumentoMapper) {
        this.tipoDocumentoMapper = tipoDocumentoMapper;
    }

    @Override
    public List<TipoDocumentoDTO> getAllTiposDocumento() {
        List<TipoDocumento> tiposDocumento = tipoDocumentoMapper.getAllTiposDocumento();
        return tipoDocumentoModelToDTO(tiposDocumento);
    }

    private List<TipoDocumentoDTO> tipoDocumentoModelToDTO(List<TipoDocumento> tiposDocumento) {
        List<TipoDocumentoDTO> dtos = new ArrayList<>();
        for (TipoDocumento tipoDocumento : tiposDocumento) {
            TipoDocumentoDTO dto = new TipoDocumentoDTO();
            dto.setId(tipoDocumento.getIdTipoDocumentoIdentidad());
            dto.setDescripcion(tipoDocumento.getDescripcionTipoDocumentoIdentidad());
            dto.setCodigo(tipoDocumento.getCodigoIeds());
            dtos.add(dto);
        }
        return dtos;
    }
}
