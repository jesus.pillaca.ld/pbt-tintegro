package com.jpillaca.apipacientesti.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
public class PacienteRequestDTO {
    private Integer idTipoDocumento;
    private String numeroDocumento;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombres;
    private Integer idSexo;
    private Date fechaNacimiento;
    private String lugarNacimiento;
    private String direccion;
    private String ubigeo;

    private Integer idPacienteA;
    private Integer idTipoDocumentoA;
    private String numeroDocumentoA;
    private String apellidoPaternoA;
    private String apellidoMaternoA;
    private String nombresA;
    private Date fechaNacimientoA;
    private Integer idParentescoA;
    private String telefonoContactoA;
    private String direccionA;
    private String ubigeoA;

}
