package com.jpillaca.apipacientesti.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TipoDocumento {
    private Integer idTipoDocumentoIdentidad;
    private String descripcionTipoDocumentoIdentidad;
    private String codigoIeds;
    private Boolean flEstado;
}
