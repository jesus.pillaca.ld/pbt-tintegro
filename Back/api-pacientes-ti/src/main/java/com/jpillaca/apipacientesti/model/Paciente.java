package com.jpillaca.apipacientesti.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Paciente {
    private Long idPaciente;
    private Integer idTipoDocIde;
    private String noDocIde;
    private String noApePat;
    private String noApeMat;
    private String noNombres;
    private Integer idSexo;
    private LocalDate feNacimiento;
    private String noLugarNacimiento;
    private String noDireccion;
    private String coUbigeo;
}
