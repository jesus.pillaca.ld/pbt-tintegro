package com.jpillaca.apipacientesti.service.impl;

import com.jpillaca.apipacientesti.mapper.PacienteMapper;
import com.jpillaca.apipacientesti.model.Paciente;
import com.jpillaca.apipacientesti.service.PacienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PacienteServiceImp implements PacienteService {

    private final PacienteMapper pacienteMapper;


    public PacienteServiceImp(PacienteMapper pacienteMapper) {
        this.pacienteMapper = pacienteMapper;
    }

    public List<Paciente> buscarPacientes(Integer idTipoDoc, String noDoc, String apePat, String apeMat, String nombres) {
        return pacienteMapper.buscarPacientes(idTipoDoc, noDoc, apePat, apeMat, nombres);
    }
}
