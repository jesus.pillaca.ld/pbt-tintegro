package com.jpillaca.apipacientesti.mapper;

import com.jpillaca.apipacientesti.dto.PacienteRequestDTO;
import com.jpillaca.apipacientesti.model.Paciente;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface PacienteMapper {
    @Select("SELECT * FROM \"Admision\".buscar_pacientes("
            + "#{idTipoDoc, jdbcType=INTEGER}, "
            + "#{noDoc, jdbcType=VARCHAR}, "
            + "#{apePat, jdbcType=VARCHAR}, "
            + "#{apeMat, jdbcType=VARCHAR}, "
            + "#{nombres, jdbcType=VARCHAR})")
    @Results({
            @Result(property = "idPaciente", column = "id_paciente"),
            @Result(property = "idTipoDocIde", column = "id_tipo_docide"),
            @Result(property = "noDocIde", column = "no_docide"),
            @Result(property = "noApePat", column = "no_apepat"),
            @Result(property = "noApeMat", column = "no_apemat"),
            @Result(property = "noNombres", column = "no_nombres"),
            @Result(property = "idSexo", column = "id_sexo"),
            @Result(property = "feNacimiento", column = "fe_nacimiento"),
            @Result(property = "noLugarNacimiento", column = "no_lugar_nacimiento"),
            @Result(property = "noDireccion", column = "no_direccion"),
            @Result(property = "coUbigeo", column = "co_ubigeo")
    })
    List<Paciente> buscarPacientes(@Param("idTipoDoc") Integer idTipoDoc,
                                   @Param("noDoc") String noDoc,
                                   @Param("apePat") String apePat,
                                   @Param("apeMat") String apeMat,
                                   @Param("nombres") String nombres);



    @Insert("SELECT insertar_paciente(" +
            "#{idTipoDocumento}, " +
            "#{numeroDocumento}, " +
            "#{apellidoPaterno}, " +
            "#{apellidoMaterno}, " +
            "#{nombres}, " +
            "#{idSexo}, " +
            "#{fechaNacimiento}, " +
            "#{lugarNacimiento}, " +
            "#{direccion}, " +
            "#{ubigeo})")
    @Options(useGeneratedKeys = true, keyProperty = "idPaciente", keyColumn = "id_paciente")
    int insertarPaciente(PacienteRequestDTO paciente);
}
