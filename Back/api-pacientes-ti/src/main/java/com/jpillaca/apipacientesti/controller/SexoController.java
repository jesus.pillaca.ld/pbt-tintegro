package com.jpillaca.apipacientesti.controller;

import com.jpillaca.apipacientesti.dto.SexoDTO;

import com.jpillaca.apipacientesti.service.SexoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/sexo")
public class SexoController {

    private final SexoService sexoService;
    public SexoController(SexoService sexoServices) {
        this.sexoService = sexoServices;
    }
    @GetMapping("/all")
    public List<SexoDTO> getAllTiposDocumento() {
        return sexoService.getAllSexo();
    }
}
