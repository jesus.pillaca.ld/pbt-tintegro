package com.jpillaca.apipacientesti.service;

import com.jpillaca.apipacientesti.dto.UbigeoDTO;
import com.jpillaca.apipacientesti.model.Ubigeo;

import java.util.List;

public interface UbigeoService {

    List<String> listarDepartamentos();

    List<String> listarProvinciasPorDepartamento(String departamento) ;

    List<UbigeoDTO> listarUbigeoPorProvinciaDistrito(String provincia);
}
