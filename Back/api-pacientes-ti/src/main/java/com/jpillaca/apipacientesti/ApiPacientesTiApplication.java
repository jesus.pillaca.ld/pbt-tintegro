package com.jpillaca.apipacientesti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPacientesTiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiPacientesTiApplication.class, args);
    }

}
