package com.jpillaca.apipacientesti.mapper;

import com.jpillaca.apipacientesti.model.TipoDocumento;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface TipoDocumentoMapper {
    TipoDocumento getTipoDocumentoById(Integer id);

    @Select("SELECT * FROM \"Admision\".tc_tipo_documento_identidad")
    @Results({
            @Result(property = "idTipoDocumentoIdentidad", column = "id_tipo_documento_identidad"),
            @Result(property = "descripcionTipoDocumentoIdentidad", column = "descripcion_tipo_documento_identidad"),
            @Result(property = "codigoIeds", column = "codigo_ieds")
    })
    List<TipoDocumento> getAllTiposDocumento();


    void insertTipoDocumento(TipoDocumento tipoDocumento);

    void updateTipoDocumento(TipoDocumento tipoDocumento);

    void deleteTipoDocumento(Integer id);
}
