package com.jpillaca.apipacientesti.mapper;

import com.jpillaca.apipacientesti.model.Parentesco;
import com.jpillaca.apipacientesti.model.Sexo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ParentescoMapper {
    @Select("SELECT * FROM \"Admision\".tc_parentesco")
    @Results({
            @Result(property = "idParentesco", column = "id_parentesco"),
            @Result(property = "noParentesco", column = "no_parentesco"),
            @Result(property = "ilActivo", column = "il_activo")
    })
    List<Parentesco> getAllParentesco();
}
