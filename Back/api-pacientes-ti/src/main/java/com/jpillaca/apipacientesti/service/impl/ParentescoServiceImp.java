package com.jpillaca.apipacientesti.service.impl;

import com.jpillaca.apipacientesti.dto.ParentescoDTO;

import com.jpillaca.apipacientesti.mapper.ParentescoMapper;
import com.jpillaca.apipacientesti.model.Parentesco;
import com.jpillaca.apipacientesti.service.ParentescoService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ParentescoServiceImp implements ParentescoService {
    private final ParentescoMapper parentescoMapper;

    public ParentescoServiceImp(ParentescoMapper parentescoMapper) {
        this.parentescoMapper = parentescoMapper;
    }

    @Override
    public List<ParentescoDTO> getAllParentesco() {
        List<Parentesco> parentesco = parentescoMapper.getAllParentesco();
        return parentescoModelToDTO(parentesco);
    }

    private List<ParentescoDTO> parentescoModelToDTO(List<Parentesco> parentescoin) {
        List<ParentescoDTO> dtos = new ArrayList<>();
        for (Parentesco parentesco : parentescoin) {
            ParentescoDTO dto = new ParentescoDTO();
            dto.setIdParentesco(parentesco.getIdParentesco());
            dto.setNoParentesco(parentesco.getNoParentesco());
            dto.setIlActivo(parentesco.getIlActivo());
            dtos.add(dto);
        }
        return dtos;
    }
}
