package com.jpillaca.apipacientesti.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PacienteAcompanante {
    private Long idPacienteAcompanante;
    private Long idPaciente;
    private Integer idTipoDocIde;
    private String noDocIde;
    private String noApePat;
    private String noApeMat;
    private String noNombres;
    private LocalDate feNacimiento;
    private Integer idParentesco;
    private String nuTelefoContacto;
    private String noDireccion;
    private String coUbigeo;
}
