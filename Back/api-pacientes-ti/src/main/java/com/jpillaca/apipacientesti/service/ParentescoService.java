package com.jpillaca.apipacientesti.service;

import com.jpillaca.apipacientesti.dto.ParentescoDTO;
import com.jpillaca.apipacientesti.dto.SexoDTO;

import java.util.List;

public interface ParentescoService {
    List<ParentescoDTO> getAllParentesco();

}
