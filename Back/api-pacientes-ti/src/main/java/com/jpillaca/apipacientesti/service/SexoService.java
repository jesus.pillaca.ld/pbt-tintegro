package com.jpillaca.apipacientesti.service;


import com.jpillaca.apipacientesti.dto.SexoDTO;
import com.jpillaca.apipacientesti.model.Sexo;

import java.util.List;

public interface SexoService {
    List<SexoDTO> getAllSexo();
}
