package com.jpillaca.apipacientesti.service;

import com.jpillaca.apipacientesti.model.Paciente;

import java.util.List;

public interface PacienteService {
    List<Paciente> buscarPacientes(Integer idTipoDoc, String noDoc, String apePat, String apeMat, String nombres);
}
