package com.jpillaca.apipacientesti.service.impl;

import com.jpillaca.apipacientesti.dto.UbigeoDTO;
import com.jpillaca.apipacientesti.dto.UbigeoDTO;
import com.jpillaca.apipacientesti.mapper.UbigeoMapper;
import com.jpillaca.apipacientesti.model.Ubigeo;
import com.jpillaca.apipacientesti.service.UbigeoService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UbigeoServiceImp implements UbigeoService {

    private final UbigeoMapper ubigeoMapper;


    public UbigeoServiceImp(UbigeoMapper ubigeoMapper) {
        this.ubigeoMapper = ubigeoMapper;
    }

    @Override
    public List<String> listarDepartamentos() {
        return ubigeoMapper.listarDepartamentos();
    }

    @Override
    public List<String> listarProvinciasPorDepartamento(String departamento) {
        String departamentoSinEspacios = departamento.trim();
        return ubigeoMapper.listarProvinciasPorDepartamento(departamentoSinEspacios);
    }

    @Override
    public List<UbigeoDTO> listarUbigeoPorProvinciaDistrito(String provincia) {
        List<Ubigeo> ubigeo = ubigeoMapper.listarUbigeoPorProvinciaDistrito(provincia);
        return ubigeoModelToDTO(ubigeo);
    }

    private List<UbigeoDTO> ubigeoModelToDTO(List<Ubigeo> ubigeoIn) {
        List<UbigeoDTO> dtos = new ArrayList<>();
        for (Ubigeo ubigeo : ubigeoIn) {
            UbigeoDTO dto = new UbigeoDTO();
            dto.setCodigoUbigeo(ubigeo.getCodigoUbigeo());
            dto.setDescripcionDistrito(ubigeo.getDescripcionDistrito());
            dtos.add(dto);
        }
        return dtos;
    }
}
