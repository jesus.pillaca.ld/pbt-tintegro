package com.jpillaca.apipacientesti.service;

import com.jpillaca.apipacientesti.dto.TipoDocumentoDTO;
import com.jpillaca.apipacientesti.model.TipoDocumento;

import java.util.List;

public interface TipoDocumentoService {
    List<TipoDocumentoDTO> getAllTiposDocumento();
}
