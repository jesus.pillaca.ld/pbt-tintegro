package com.jpillaca.apipacientesti.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ubigeo {
    private String descripcionDepartamento;
    private String descripcionProvincia;
    private String descripcionDistrito;
    private Boolean flEstado;
    private String codigoUbigeo;
    private String codigoDepartamento;
    private String codigoProvincia;
    private String codigoDistrito;
}
