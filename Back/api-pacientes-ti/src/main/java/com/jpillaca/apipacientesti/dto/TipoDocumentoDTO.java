package com.jpillaca.apipacientesti.dto;

import lombok.Data;

@Data
public class TipoDocumentoDTO {
    private Integer id;
    private String descripcion;
    private String codigo;
}
