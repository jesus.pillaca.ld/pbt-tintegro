export interface Parentesco {

      idParentesco?: number;
      noParentesco?: string;
      ilActivo?: boolean;
}
