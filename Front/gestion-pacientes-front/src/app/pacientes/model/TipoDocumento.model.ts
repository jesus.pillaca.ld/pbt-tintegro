export interface TipoDocumento {
  id?: number;
  descripcion?: string;
  codigo?: string;
}
