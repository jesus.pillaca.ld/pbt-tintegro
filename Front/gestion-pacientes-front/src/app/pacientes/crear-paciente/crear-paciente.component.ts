import { Component, Inject } from '@angular/core';
import { TipoDocumento } from '../model/TipoDocumento.model';
import { Sexo } from '../model/Sexo.model';
import { Parentesco } from '../model/Parentesco.model';
import { TipoDocumentoService } from './../services/tipo-documento.service';
import { ParentescoService } from '../services/parentesco.service';
import { SexoService } from './../services/sexo.service';
import { UbigeoDTO, UbigeoService } from '../services/ubigeo.service';
import { MAT_DATE_LOCALE } from '@angular/material/core';

@Component({
  selector: 'app-crear-paciente',
  templateUrl: './crear-paciente.component.html',
  styleUrl: './crear-paciente.component.scss',

})
export class CrearPacienteComponent {
  selectedTipoDocumento: any;
  tiposDocumento: TipoDocumento[] = [];
  selectedTipoDocumentoP: any;
  tiposDocumentoP: TipoDocumento[] = [];
  selectedSexo: any;
  sexoList: Sexo[] = [];
  selectedParentesco: any;
  parentescoList: Parentesco[] = [];
  numeroDocumento: string = "";
  numeroDocumentoP: string = "";


  departamentos: string[] = [];
  provincias: string[] = [];
  distritos: UbigeoDTO[] = [];

  departamentoSeleccionado: string = '';
  provinciaSeleccionada: string = '';
  distritoSeleccionado: string = ''
  departamentosA: string[] = [];
  provinciasA: string[] = [];
  distritosA: UbigeoDTO[] = [];

  departamentoSeleccionadoA: string = '';
  provinciaSeleccionadaA: string = '';
  distritoSeleccionadoA: string = ''

// Variables para los datos del paciente

apellidoPaterno: string = '';
apellidoMaterno: string = '';
nombres: string = '';
fechaNacimiento: Date = new Date();
edad: number = 0;
lugarNacimiento: string = '';
direccion: string = '';

// Variables para los datos del acompañante
apellidoPaternoA: string = '';
apellidoMaternoA: string = '';
nombresA: string = '';
fechaNacimientoA: Date = new Date();
edadA: number = 0;
telefonoContacto: string = '';
direccionA: string = '';

  constructor(
    private tipoDocumentoService: TipoDocumentoService,
    private sexoService: SexoService,
    private parentescoService: ParentescoService,
    private ubigeoService: UbigeoService
    ) {

     }


  ngOnInit(): void {

    this.getTiposDocumento();
    this.getSexoList();
    this.getParentescoList();
    this.listarDepartamentos();
    this.listarDepartamentosA();

  }

  getTiposDocumento(): void {
    this.tipoDocumentoService.getAllTiposDocumento().subscribe({
      next: (response: TipoDocumento[]) => {
        this.tiposDocumento = response;
        this.tiposDocumentoP = response;
      },
      error: (err: any) => {
        console.error('Error al obtener tipos de documento:', err);
      },
      complete: () => {
        console.log('Tipos de documento obtenidos con éxito');
      }
    });

  }
  getSexoList(): void {
    this.sexoService.getAllSexo().subscribe({
      next: (response: Sexo[]) => {
        this.sexoList = response;
      },
      error: (err: any) => {
        console.error('Error al obtener lista de sexo:', err);
      },
      complete: () => {
        console.log('Lista de sexo obtenidos con éxito');
      }
    });

  }
  getParentescoList(): void {
    this.parentescoService.getAllParentesco().subscribe({
      next: (response: Parentesco[]) => {
        this.parentescoList = response;
      },
      error: (err: any) => {
        console.error('Error al obtener Lista de parentescos:', err);
      },
      complete: () => {
        console.log('Lista de Parentesco obtenidos con éxito');
      }
    });

  }
  listarDepartamentos() {
    this.ubigeoService.listarDepartamentos().subscribe(
      data => {
        this.departamentos = data;
      },
      error => {
        console.error('Error al obtener los departamentos:', error);
      }
    );
  }

  listarProvinciasPorDepartamento(departamento: string) {
    this.ubigeoService.listarProvinciasPorDepartamento(departamento).subscribe(
      data => {
        this.provincias = data;
        this.distritos = []; // Reiniciar los distritos al cambiar de provincia
      },
      error => {
        console.error('Error al obtener las provincias:', error);
      }
    );
  }

  listarUbigeoPorProvinciaDistrito(provincia: string) {
    this.ubigeoService.listarUbigeoPorProvinciaDistrito(provincia).subscribe(
      data => {
        this.distritos = data;
      },
      error => {
        console.error('Error al obtener los distritos:', error);
      }
    );
  }
  listarDepartamentosA() {
    this.ubigeoService.listarDepartamentos().subscribe(
      data => {
        this.departamentosA = data;
      },
      error => {
        console.error('Error al obtener los departamentos:', error);
      }
    );
  }

  listarProvinciasPorDepartamentoA(departamento: string) {
    this.ubigeoService.listarProvinciasPorDepartamento(departamento).subscribe(
      data => {
        this.provinciasA = data;
        this.distritosA = []; // Reiniciar los distritos al cambiar de provincia
      },
      error => {
        console.error('Error al obtener las provincias:', error);
      }
    );
  }

  listarUbigeoPorProvinciaDistritoA(provincia: string) {
    this.ubigeoService.listarUbigeoPorProvinciaDistrito(provincia).subscribe(
      data => {
        this.distritosA = data;
      },
      error => {
        console.error('Error al obtener los distritos:', error);
      }
    );
  }

  guardarDatos(): void {
    console.log('Datos guardados:', {
      selectedTipoDocumento: this.selectedTipoDocumento,
      numeroDocumento: this.numeroDocumento,
      apellidoPaterno: this.apellidoPaterno,
      apellidoMaterno: this.apellidoMaterno,
      nombres: this.nombres,
      selectedSexo: this.selectedSexo,
      fechaNacimiento: this.fechaNacimiento,
      edad: this.edad,
      lugarNacimiento: this.lugarNacimiento,
      direccion: this.direccion,
      departamentoSeleccionado: this.departamentoSeleccionado,
      provinciaSeleccionada: this.provinciaSeleccionada,
      distritoSeleccionado: this.distritoSeleccionado,
      selectedTipoDocumentoP: this.selectedTipoDocumentoP,
      numeroDocumentoP: this.numeroDocumentoP,
      apellidoPaternoA: this.apellidoPaternoA,
      apellidoMaternoA: this.apellidoMaternoA,
      nombresA: this.nombresA,
      fechaNacimientoA: this.fechaNacimientoA,
      edadA: this.edadA,
      selectedParentesco: this.selectedParentesco,
      telefonoContacto: this.telefonoContacto,
      direccionA: this.direccionA,
      departamentoSeleccionadoA: this.departamentoSeleccionadoA,
      provinciaSeleccionadaA: this.provinciaSeleccionadaA,
      distritoSeleccionadoA: this.distritoSeleccionadoA,
    });
  }

  calcularEdad(): void {
    if (this.fechaNacimiento) {
      const diferencia = Date.now() - new Date(this.fechaNacimiento).getTime();
      const edadFecha = new Date(diferencia);
      this.edad = Math.abs(edadFecha.getUTCFullYear() - 1970);
    }
  }
  calcularEdadA(): void {
    if (this.fechaNacimientoA) {
      const diferencia = Date.now() - new Date(this.fechaNacimientoA).getTime();
      const edadFecha = new Date(diferencia);
      this.edadA = Math.abs(edadFecha.getUTCFullYear() - 1970);
    }
  }


}
