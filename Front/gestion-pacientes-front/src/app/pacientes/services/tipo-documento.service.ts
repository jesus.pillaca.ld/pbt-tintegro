import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TipoDocumento } from '../model/TipoDocumento.model';
import { API_URL } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class TipoDocumentoService {

  private apiUrl = `${API_URL}/api`;

  constructor(private http: HttpClient) { }

  getAllTiposDocumento(): Observable<TipoDocumento[]> {
    return this.http.get<TipoDocumento[]>(`${this.apiUrl}/tipo-documento/all`);
  }
}
