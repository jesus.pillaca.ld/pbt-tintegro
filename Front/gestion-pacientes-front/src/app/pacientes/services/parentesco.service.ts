import { Injectable } from '@angular/core';
import { API_URL } from '../constants';
import { HttpClient } from '@angular/common/http';
import { Parentesco } from '../model/Parentesco.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ParentescoService {

  private apiUrl = `${API_URL}/api`;

  constructor(private http: HttpClient) { }

  getAllParentesco(): Observable<Parentesco[]> {
    return this.http.get<Parentesco[]>(`${this.apiUrl}/parentesco/all`);
  }
}
