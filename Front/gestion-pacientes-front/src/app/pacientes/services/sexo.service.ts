import { Injectable } from '@angular/core';
import { API_URL } from '../constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Sexo } from '../model/Sexo.model';

@Injectable({
  providedIn: 'root'
})
export class SexoService {

  private apiUrl = `${API_URL}/api`;

  constructor(private http: HttpClient) { }

  getAllSexo(): Observable<Sexo[]> {
    return this.http.get<Sexo[]>(`${this.apiUrl}/sexo/all`);
  }
}
