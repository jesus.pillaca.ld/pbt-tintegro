import { Injectable } from '@angular/core';
import { API_URL } from '../constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UbigeoService {

  private apiUrl = `${API_URL}/api`;

  constructor(private http: HttpClient) { }

  listarDepartamentos(): Observable<string[]> {
    const url = `${this.apiUrl}/ubigeo/departamentos`;
    return this.http.get<string[]>(url);
  }

  listarProvinciasPorDepartamento(departamento: string): Observable<string[]> {
    const url = `${this.apiUrl}/ubigeo/provincias/${departamento}`;
    return this.http.get<string[]>(url);
  }

  listarUbigeoPorProvinciaDistrito(provincia: string): Observable<UbigeoDTO[]> {
    const url = `${this.apiUrl}/ubigeo/ubigeo/${provincia}`;
    return this.http.get<UbigeoDTO[]>(url);
  }
}

export interface UbigeoDTO {
  codigoUbigeo: string;
  descripcionDistrito: string;
}
