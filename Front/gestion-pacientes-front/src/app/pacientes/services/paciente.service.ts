import { Injectable } from '@angular/core';
import { API_URL } from '../constants';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Paciente } from '../model/Paciente.model';

@Injectable({
  providedIn: 'root'
})
export class PacienteService{

  private apiUrl = `${API_URL}/api`;

  constructor(private http: HttpClient) { }
  buscarPacientes(idTipoDoc?: number, noDoc?: string, apePat?: string, apeMat?: string, nombres?: string): Observable<Paciente[]> {
    let params = new HttpParams();
    if (idTipoDoc !== undefined) {
      params = params.append('idTipoDoc', idTipoDoc.toString());
    }
    if (noDoc !== undefined) {
      params = params.append('noDoc', noDoc);
    }
    if (apePat !== undefined) {
      params = params.append('apePat', apePat);
    }
    if (apeMat !== undefined) {
      params = params.append('apeMat', apeMat);
    }
    if (nombres !== undefined) {
      params = params.append('nombres', nombres);
    }

    return this.http.get<Paciente[]>(`${this.apiUrl}/paciente/find`, { params: params });
  }
}
