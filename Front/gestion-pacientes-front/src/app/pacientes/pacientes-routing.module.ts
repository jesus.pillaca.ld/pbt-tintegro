import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ListarPacienteComponent } from "./listar-paciente/listar-paciente.component";
import { CrearPacienteComponent } from "./crear-paciente/crear-paciente.component";

const routes: Routes = [
  { path: 'ListarPaciente', component: ListarPacienteComponent },
  { path: 'CrearPaciente', component: CrearPacienteComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacientesRoutingModule { }
