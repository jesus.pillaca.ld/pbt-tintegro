import { AfterViewInit, Component, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { TipoDocumento } from '../model/TipoDocumento.model';
import { TipoDocumentoService } from './../services/tipo-documento.service';
import { Observable } from 'rxjs';
import { PacienteService } from '../services/paciente.service';
import { Paciente } from '../model/Paciente.model';


@Component({
  selector: 'app-listar-paciente',
  templateUrl: './listar-paciente.component.html',
  styleUrl: './listar-paciente.component.scss'
})
export class ListarPacienteComponent implements AfterViewInit{
  tiposDocumento: TipoDocumento[] = [];
  selectedTipoDocumento: any;
  numeroDocumento: string = "";
  apellidoPaterno: string = "";
  apellidoMaterno: string = "";
  nombres: string = "";
  dataPacientes: Paciente[] = [];
  displayedColumns: string[] = ['noNombres','apaterno','amaterno' ,'idTipoDocIde','noDocIde', 'feNacimiento', 'edit',  'delet'];
  dataSource :any;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private tipoDocumentoService: TipoDocumentoService,
    private pacienteService: PacienteService) {
      this.dataSource = new MatTableDataSource<Paciente>([]);
     }

  ngAfterViewInit() {
    this.dataSource.paginator?.firstPage();  }
  ngOnInit(): void {

    this.listarPacientes();
    this.getTiposDocumento();
  }

  getTiposDocumento(): void {
    this.tipoDocumentoService.getAllTiposDocumento().subscribe({
      next: (response: TipoDocumento[]) => {
        this.tiposDocumento = response;
      },
      error: (err: any) => {
        console.error('Error al obtener tipos de documento:', err);
      },
      complete: () => {
        console.log('Tipos de documento obtenidos con éxito');
      }
    });

  }
  listarPacientes(): void {
    const tipoDocumento = 0;
    const noDocumento = '0';
    const apePaterno = '0';
    const apeMaterno =  '0';
    const nombres = '0';

    this.pacienteService.buscarPacientes(tipoDocumento, noDocumento, apePaterno, apeMaterno, nombres)
      .subscribe(pacientes => {
        this.dataSource = new MatTableDataSource<Paciente>(pacientes);


        console.log('Pacientes Listados:', pacientes);
      });
  }
  buscarPacientes(): void {
    // Asignamos cero a los campos vacíos
    const tipoDocumento = this.selectedTipoDocumento || 0;
    const noDocumento = this.numeroDocumento || '0';
    const apePaterno = this.apellidoPaterno || '0';
    const apeMaterno = this.apellidoMaterno || '0';
    const nombres = this.nombres || '0';

    // Llama al servicio para buscar pacientes con los filtros proporcionados
    this.pacienteService.buscarPacientes(tipoDocumento, noDocumento, apePaterno, apeMaterno, nombres)
      .subscribe(pacientes => {
        this.dataSource = new MatTableDataSource<Paciente>(pacientes);

        // Maneja la respuesta del servicio aquí, por ejemplo, podrías asignar los resultados a una variable en el componente
        console.log('Pacientes encontrados:', pacientes);
      });
  }
  limpiarCampos(): void {
    // Limpia los campos del formulario
    this.selectedTipoDocumento = 0;
    this.numeroDocumento = '';
    this.apellidoPaterno = '';
    this.apellidoMaterno = '';
    this.nombres = '';
    this.listarPacientes();
  }


}

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}


